<img src="files/logo_readme.jpeg" width="250px" height="200px" alt="react graphql nodejs Logo">

# Frontend for the TodoQL API
An react app design to consume the TodoQL api

### Clone

- Clone this repo to your local machine using

```shell
git clone https://gitlab.com/mateusgpereira/frontend.git
```

### Instalation
- Install the dependencies with yarn command in the app folder
```shell
cd frontend
yarn
```

### Run
- To start the app use the command below inside the app's directory:
```shell
yarn start
```

The Api is not finished, for now is possible to List, Create, Update and Delete Todos
And Create and Login with Users

There are lots of improvments to work on about the user interations and error handlers.

## /auth

| For now, on the **/auth** route, you can:
* sign up
* login (u need to use 7 characters minumum password)

<img src="files/login.png" alt="Login Page">

## /todos

|On the **/todos** page, you can:
* add new todos
* update the completed status by clicking the checkbox
* and delete the todo clicking on the trash icon.

<img src="files/todo_page.png" alt="Login Page">
