import styled from 'styled-components'

const DefaultHeader = styled.header`
  color: #fff;
  display: flex;
  justify-content: space-between;
  width: 100%;

  h1 {
    font-weight: 600;
  }

  button {
    background-color: #000;
    border: none;
    border-radius: 25px;
    color: #fff;
    font-weight: 500;
    padding: 10px 15px;

    &:hover {
      background-color: #222;
    }
  }
`

export default DefaultHeader
