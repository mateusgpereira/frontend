import styled from 'styled-components'

const Container = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 0px auto;
  max-width: 760px;
  padding: 30px;
  height: 100vh;

  @media(max-width: 600px) {
    max-width: 100vw;
    padding: 10px;
    width: 100%;
  }
}
`

export default Container
