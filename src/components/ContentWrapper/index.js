import styled from 'styled-components'

const ContentWrapper = styled.main`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: -webkit-fill-available;
  width: 100%;
`

export default ContentWrapper
