import React, { Component } from 'react'
import { FaTrash } from 'react-icons/fa'
import DefaultHeader from '../../components/Header'
import ContentWrapper from '../../components/ContentWrapper'
import AuthContext from '../../context/auth-context'

import graphql from '../../services/api'

import { Form, List } from './styles'

class Todo extends Component {
  static contextType = AuthContext

  state = {
    todos: [],
    newTodo: '',
    headers: { Authorization: `Bearer ${this.context.token}` }
  }

  componentDidMount() {
    this.fetchTodos()
  }

  fetchTodos = async () => {
    const { headers } = this.state
    const body = {
      query: `
        query {
          todos {
            _id
            description
            completed
          }
        }
      `
    }

    try {
      const { data: res } = await graphql.post('', body, { headers })
      if (res.data.todos) {
        this.setState({ todos: res.data.todos })
      }
    } catch (err) {
      console.log(err)
    }
  }

  handleInputChange = (e) => {
    this.setState({ newTodo: e.target.value })
  }

  handleSubmit = async (e) => {
    e.preventDefault()
    const { newTodo, todos, headers } = this.state

    if (newTodo.trim() === '') {
      console.log('Todo cannot be empty')
      return null
    }

    const body = {
      query: `
        mutation {
          createTodo(todoInput: {description: "${newTodo}"}) {
            _id
            description
            completed
          }
        }
      `
    }

    try {
      const { data: res } = await graphql.post('', body, { headers })
      const createdTodo = res.data.createTodo
      if (createdTodo) {
        todos.push(createdTodo)
        this.setState({ todos, newTodo: '' })
      }
    } catch (err) {
      console.log(err)
    }
  }

  handleChangeTodo = (e) => {
    const { todos, headers } = this.state
    const { id: todoId, checked: completed } = e.target

    todos.forEach((todo) => {
      if (todo._id === todoId) {
        todo.completed = completed
      }
    })
    this.setState({ todos })

    const body = {
      query: `
        mutation {
          updateTodo(todoInput: {_id: "${todoId}",completed: ${completed}}) {
            _id
          }
        }
      `
    }

    graphql
      .post('', body, { headers })
      .then((res) => {
        console.log(res)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  handleDeleteTodo = async (todoId) => {
    const { headers, todos } = this.state

    const body = {
      query: `
        mutation {
          deleteTodo(id: "${todoId}") {
            _id
          }
        }
      `
    }

    try {
      const { data: res } = await graphql.post('', body, { headers })
      if (res.data.deleteTodo._id) {
        this.setState({ todos: todos.filter((todo) => todo._id !== todoId) })
      }
    } catch (err) {
      console.log(err)
    }
  }

  render() {
    const { todos } = this.state
    return (
      <AuthContext.Consumer>
        {(context) => {
          return (
            <>
              <DefaultHeader>
                <h1>Todo App</h1>
                <button type="button" onClick={context.logout}>
                  Logout
                </button>
              </DefaultHeader>
              <ContentWrapper>
                <Form onSubmit={this.handleSubmit}>
                  <input
                    type="text"
                    value={this.state.newTodo}
                    onChange={this.handleInputChange}
                    placeholder="Type your todo here"
                  />
                  <button type="submit">Add Todo</button>
                </Form>
                <List>
                  {todos.map((todo) => (
                    <li key={todo._id}>
                      <span>
                        <input
                          type="checkbox"
                          checked={todo.completed}
                          id={todo._id}
                          onChange={this.handleChangeTodo}
                        />
                        <label htmlFor={todo._id}>{todo.description}</label>
                      </span>
                      <button
                        type="button"
                        onClick={() => this.handleDeleteTodo(todo._id)}
                      >
                        <FaTrash color="#5680e9" size={20} />
                      </button>
                    </li>
                  ))}
                </List>
              </ContentWrapper>
            </>
          )
        }}
      </AuthContext.Consumer>
    )
  }
}

export default Todo
