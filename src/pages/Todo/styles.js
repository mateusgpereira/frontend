import styled from 'styled-components'

const Form = styled.form`
  display: flex;
  flex-direction: row;
  margin-top: 30px;
  width: 80%;
  input {
    border: 1px solid #eee;
    border-radius: 4px;
    color: #222;
    flex-grow: 1;
    font-size: 16px;
    padding: 10px 15px;
  }

  button {
    background-color: #000;
    border: none;
    border-radius: 4px;
    color: #fff;
    font-size: 20px;
    font-weight: 500;
    padding: 10px 15px;
    margin-left: 10px;

    &:hover {
      background-color: #222;
    }
  }
`

const List = styled.ul`
  -webkit-box-shadow: 0px 0px 5px 2px rgba(54, 52, 54, 0.52);
  -moz-box-shadow: 0px 0px 5px 2px rgba(54, 52, 54, 0.52);
  box-shadow: 0px 0px 5px 2px rgba(54, 52, 54, 0.52);
  list-style: none;
  margin-top: 30px;
  padding: 20px 40px;
  width: 80%;
  li {
    align-items: center;
    background-color: #fff;
    border-radius: 15px;
    color: #333;
    display: flex;
    flex-direction: row;
    font-family: -apple-system, system-ui, BlinkMacSystemFont, 'Segoe UI',
      Roboto, 'Helvetica Neue', Arial, sans-serif;
    font-size: 20px;
    font-weight: 500;
    justify-content: space-between;
    margin: 10px 0;
    padding: 10px 10px;

    button {
      background: none;
      border: none;
      margin: 0 5px;
      &:hover,
      &:active {
        opacity: 0.65;
      }
    }

    span {
      label {
        padding: 0 10px;
        word-break: break-all;
      }
    }
  }
`

export { Form, List }
