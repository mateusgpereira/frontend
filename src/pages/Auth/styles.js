import styled from 'styled-components'

const LoginForm = styled.form`
  background-color: #5680e9;
  -webkit-box-shadow: 10px 10px 5px 0px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 10px 10px 5px 0px rgba(0, 0, 0, 0.75);
  box-shadow: 10px 10px 5px 0px rgba(0, 0, 0, 0.75);
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 30px;
  width: 70%;

  button {
    background-color: #000;
    border: none;
    border-radius: 25px;
    color: #fff;
    font-size: 20px;
    font-weight: 500;
    padding: 10px 15px;
    margin: 20px 40px;

    &:hover {
      background-color: #222;
    }
  }
`

const FormField = styled.div`
  align-items: center;
  color: #fff;
  display: flex;
  flex-direction: column;
  font-size: 20px;
  font-weight: 600;
  margin: 10px 0px;

  input {
    background-color: #fff;
    border: none;
    border-radius: 4px;
    color: #222;
    font-size: 16px;
    font-weight: 500;
    height: 35px;
    margin: 10px 40px;
    text-align: center;
    width: -webkit-fill-available;
  }
`

export { LoginForm, FormField }
