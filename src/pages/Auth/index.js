import React, { Component } from 'react'
import DefaultHeader from '../../components/Header'
import ContentWrapper from '../../components/ContentWrapper'
import { LoginForm, FormField } from './styles'
import graphqlApi from '../../services/api'
import AuthContext from '../../context/auth-context'

class Auth extends Component {
  static contextType = AuthContext

  state = {
    isLogin: true
  }

  constructor(props) {
    super(props)
    this.emailEl = React.createRef()
    this.passEl = React.createRef()
  }

  handleSwitch = () => {
    this.setState((prevState) => {
      return {
        isLogin: !prevState.isLogin
      }
    })
  }

  handleSubmit = async (e) => {
    e.preventDefault()
    const email = this.emailEl.current.value.trim()
    const password = this.passEl.current.value.trim()

    if (email.length === 0 || password.length === 0) {
      return
    }

    let body = {
      query: `
        query {
          login(email: "${email}", password: "${password}") {
            userId
            token
            tokenExpiration
          }
        }
      `
    }

    if (!this.state.isLogin) {
      body = {
        query: `
          mutation {
            createUser(userInput: {email: "${email}", password: "${password}"}) {
              _id
              email
            }
          }
        `
      }
    }

    const { data: response } = await graphqlApi.post('', body)

    if (response.data.login) {
      this.context.login(
        response.data.login.token,
        response.data.login.userId,
        response.data.login.tokenExpiration
      )
    }
  }

  render() {
    const { isLogin } = this.state
    return (
      <>
        <DefaultHeader>
          <h1>Login</h1>
          <button type="button" onClick={this.handleSwitch}>
            {isLogin ? 'Sign Up' : 'Login'}
          </button>
        </DefaultHeader>
        <ContentWrapper>
          <LoginForm onSubmit={this.handleSubmit}>
            <FormField>
              <label htmlFor="email">E-Mail</label>
              <input
                type="email"
                id="email"
                ref={this.emailEl}
                placeholder="youremail@address.com"
              />
            </FormField>
            <FormField>
              <label htmlFor="password">Password</label>
              <input
                type="password"
                id="password"
                ref={this.passEl}
                placeholder="***********"
              />
            </FormField>
            <button type="submit">{isLogin ? 'Login' : 'Sign Up'}</button>
          </LoginForm>
        </ContentWrapper>
      </>
    )
  }
}

export default Auth
