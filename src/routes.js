import React, { Component } from 'react'
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import Auth from './pages/Auth'
import Todo from './pages/Todo'
import Container from './components/Container'
import AuthContext from './context/auth-context'

class Routes extends Component {
  state = {
    token: null,
    userId: null
  }

  componentDidMount() {
    const localToken = localStorage.getItem('token')
    if (localToken) {
      this.setState({ token: localToken })
    }
  }

  login = (token, userId, tokenExpiration) => {
    this.setState({ token, userId })
    localStorage.setItem('token', token)
  }

  logout = () => {
    this.setState({ token: null, userId: null })
    localStorage.clear()
  }

  render() {
    return (
      <BrowserRouter>
        <AuthContext.Provider
          value={{
            token: this.state.token,
            userId: this.state.userId,
            login: this.login,
            logout: this.logout
          }}
        >
          <Container>
            <Switch>
              {!this.state.token && (
                <>
                  <Redirect from="/" to="/auth" exact />
                  <Route path="/auth" component={Auth} />
                </>
              )}
              {this.state.token && (
                <>
                  <Redirect from="/" to="/todos" exact />
                  <Redirect from="/auth" to="/todos" />
                  <Route path="/todos" component={Todo} />
                </>
              )}
            </Switch>
          </Container>
        </AuthContext.Provider>
      </BrowserRouter>
    )
  }
}

export default Routes
