import axios from 'axios'

const graphqlApi = axios.create({
  baseURL: 'http://localhost:8080/graphql',
  headers: {
    'Content-Type': 'application/json'
  }
})

export default graphqlApi
