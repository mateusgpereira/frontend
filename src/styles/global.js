import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
  * {
    box-sizing: border-box;
    margin: 0;
    outline: 0;
    padding: 0;
  }

  html, body, #root {
    min-height: 100vh;
  }

  body {
    background: #5680e9;
    -webkit-font-smoothing: antialiased !important;
  }

  body, input, button {
    color: #eee;
    font-family: Roboto, Arial, Helvetica, sans-serif;
    font-size: 14px;
  }

  button {
    cursor: pointer;
  }
`
