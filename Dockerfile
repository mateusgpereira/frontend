FROM node:12.18.0-alpine3.12

RUN mkdir /app
WORKDIR /app

COPY package.json yarn.lock /app/

RUN yarn

EXPOSE 3000

CMD ["yarn", "start"]
