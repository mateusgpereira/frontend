module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
    'prettier',
    'prettier/react'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'prettier'
  ],
  rules: {
    "semi": ["error", "never"],
    "class-methods-use-this": "off",
    "no-param-reassign": "off",
    "camelcase": "off",
    "no-unused-vars": ["error", { "argsIgnorePattern": "next"}],
    "consistent-return": "off",
    "max-len": ["error", { "code": 120 }],
    "no-restricted-globals": "off",
    "react/jsx-filename-extension": ["warn", { "extensions": [".js", ".jsx"] }],
    "react/destructuring-assignment": ["off"],
    "import/prefer-default-export": "off",
    "react/state-in-constructor": "off",
    "react/prop-types": "off",
    "no-underscore-dangle": ["error", { "allow": ["_doc", "_id"] }],
    "prettier/prettier": "error",
  },
};
